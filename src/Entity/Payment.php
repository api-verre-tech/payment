<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\PaymentRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ApiResource(
 *     collectionOperations={"post", "get"},
 *     itemOperations={
 *         "get",
 *     }
 * )
 * @ORM\Entity(repositoryClass=PaymentRepository::class)
 * @ApiFilter(SearchFilter::class, properties={"id": "exact", "fkid_c": "exact"})
 */
class Payment
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Positive
     * @Assert\NotNull
     */
    private $fkid_c;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     *      min = 2,
     *      max = 20
     * )
     * @Assert\NotNull
     */
    private $info;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\Type("\DateTimeInterface")
     */
    private $payment_date;

    /**
     * @ORM\Column(type="float")
     * @Assert\Type("float")
     */
    private $amount;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFkidC(): ?int
    {
        return $this->fkid_c;
    }

    public function setFkidC(int $fkid_c): self
    {
        $this->fkid_c = $fkid_c;

        return $this;
    }

    public function getInfo(): ?string
    {
        return $this->info;
    }

    public function setInfo(string $info): self
    {
        $this->info = $info;

        return $this;
    }

    public function getPaymentDate(): ?\DateTimeInterface
    {
        return $this->payment_date;
    }

    public function setPaymentDate(\DateTimeInterface $payment_date): self
    {
        $this->payment_date = $payment_date;

        return $this;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }
}
