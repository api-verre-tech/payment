<?php

namespace App\DataFixtures;

use DateTime;
use App\Entity\Payment;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $payment = new Payment();
        $payment->setFkidC(1);
        $payment->setInfo("CB");
        $payment->setPaymentDate(new \DateTime('now'));
        $payment->setAmount(144.50);
        $manager->persist($payment);

        $payment = new Payment();
        $payment->setFkidC(1);
        $payment->setInfo("Espece");
        $payment->setPaymentDate(new \DateTime('now'));
        $payment->setAmount(34.5);
        $manager->persist($payment);

        $manager->flush();
    }
}
